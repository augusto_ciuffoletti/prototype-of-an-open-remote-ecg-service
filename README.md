# Remote EKG using low cost hardware and WebSockets

The aim of this project is to demonstrate that it is possible to implement a low cost remote EKG probe with a limited usage of proprietary technology at a cost that is in the range of tens of euros (or dollars). The full details of the project are in a [published paper](https://doi.org/10.3390/fi11040101), and a guide for assemblig the prototype and demonstratin its operation is available as a [screencast](https://vimeo.com/338839466) .

## Functionality
The project delivers a three leads EKG (a single trace) between a sensor and a browser across the Internet. The two terminal devices, the probe and the PC running the browser, need to be connected to the Internet; for the probe the connection is by WiFi.
We have obtained interesting results with a probe frequency of 250 Hz, and bouncing the signal from Europe to USA and back.

## Layout
The project includes three applications:
- an Arduino sketch that encodes the analog output from the ECG probe
- an ESP8266 sketch that implements a websocket with a remote server and relays the ECG to the server
- a Python web application that provides the web page containing the code (app) to display the EKG. The data from the client are then forwarded to the browser hosting the visualization app.
## Materials
You need a PC with the following software intalled:
- Arduino IDE, with the package for the ESP8266 family and the needed libraries
- Heroku CLI, and an heroku account
You need the following hardware:
- An Olimex EKG/EMG board (50 Euro)
- Arduino UNO R3 (10 Euro)
- ESP 12E (10 Euro)
- Arduino protoshield (5 Euro)
- Breadboard wires

## Flash the ESP 12E
Dowload the repository
Include in the "ws_8266" directory a "config.h" file with the following content:
```
	const char ekgServerIP[]="192.168.1.33";
	const int ekgServerPort=8080;
	const char ekgServerPath[]="/in/user123";
	const char ESSID[]="ESSID";
	const char WIFI_PSK[]="PSK";
```
Replace ESSID and PSK with those appropriate for your WiFi. 

ekgServerPort are appropriate for running the server locally, not on a Heroku instance. The ecgServerIP is that of the web server, in local operation it is your PC, problably connected to the same WiFi intranet as the probe.

Now you are ready to flash the **ws_ESP8266** sketch on your ESP12E using a USB cable (see online tutorials). The blue LED is initially off, and next turns on.

TEST: opening the Serial Monitor (115200 baud) of the Arduino IDE you should read:
```
Booting....

scandone
scandone
state: 0 -> 2 (b0)
state: 2 -> 3 (0)
state: 3 -> 5 (10)
add 0
aid 6
cnt 

connected with ESSID, channel x
dhcp client start...
ip:192.168.xxx.xxx,mask:255.255.xxx.xxxx,gw:192.168.xxx.xxx
Websocket disconnected!
Websocket disconnected!
Websocket disconnected!
Websocket disconnected!
Websocket disconnected!
```
The initial line may be incomplete, replace ESSID and addresses as in your WiFi LAN.

## Flash the Arduino

Connect the Arduino to the  serial line and flash the sketch **ecg-encoder**.

TEST: on the Serial Monitor (115200 baud) you should read:
```
⸮p⸮⸮0⸮|,
%⸮P⸮~⸮⸮⸮q"⸮a⸮1?⸮`⸮&0t⸮?BȔ⸮@⸮⸮&2_⸮򡀒0⸮@0⸮0
,⸮⸮⸮?⸮⸮
⸮~⸮⸮(?4,d$⸮⸮⸮~⸮!2?⸮% "⸮$⸮`⸮⸮⸮⸮L?⸮!000:00:00.003  413382366348265240Q
000:00:00.007  286310321323274243Q
000:00:00.011  234252268280259238Q
000:00:00.015  234234247260260251Q
...
```
## Assemble the stack

Stack the four boards:
- the Olimex board over the Arduino
- the protoshield hosting a small breadboard over the Olimex board
- insert the ESP12E in the breadbord of the protoshield
Implement the following connections between the protoshield pins and the ESP12E:
- Gnd and Gnd
- 5V (Arduino pin) with Vin (ESP12E pin)
- Tx and Rx
- Rx and Tx
The last two should be mounted only after flashing the boards, and removed while flashing

## Create the local server

This section guides you to the installation of the server on the  PC where you download the repository. This is of interest mainly for developers and those that want to see how it works.

In order for this to work, you should have installed python 2.7, pip and virtualenv.

The config.h file should contain the address of your PC (```192.168.x.x``` or a similar private address) and port ```5000``` in ```ecgServer``` specifications.

- move into the heroku_server directory
```
$ cd heroku_server
```
- create a new environment
```
$ virtualenv ecg
$ source ecg/bin/activate
```
- install the dependencies
```
$ pip install -r requirements.txt
```
Now launch the server locally:
```
$ heroku local web
```
See testing suite after next section.

## Deploy an heroku server

This section guides you to the installation of the ecg-server on an  Heroku instance.  For this you need to have a Heroku account (free plan).

- move into the heroku_server directory and initialize as git repository
```
$ cd heroku_server
$ git init
```
(note for the developer, it may be appropriate to copy the directory elsewhere to avoid git nesting)

- index the relevant files and commit
```
$ git add ecg_server.py Procfile requirements.txt runtime.txt templates/
$ git commit -m "Ready to deploy"
```
- create the remote Heroku instance, take a note of the random instance name, and connect as a git remote for deployment
```
$ heroku create
```
- tether the repo to the Heroku instance
``` 
$ heroku git:remote -a instance-name-00000
```
- push the code on your new instance
```
$ git push heroku master
```
- Check that everything is ok, and your server is up and running
- Go to the Arduino IDE, modify the config.h file of the ws-ESP8266 sketch with ecgServerPort 80, and the name of the instance for the ecgServerIP, and flash the ESP21E

## Testing suite

- Push the reset button on the ESP 12E.

- Go the browser on a PC that can reach the server (possibly on that same). And type the URL. For a local installation the port is 5000, for the Heroku it is not needed to specify one. Do not use https (which is not free on Heroku) , but plain http.

The plot should start to display o the browser, while the blue LED on the ESP12E should start flashing. 
