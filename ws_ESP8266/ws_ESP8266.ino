// Using https://github.com/Links2004/arduinoWebSockets
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <WebSocketsClient.h>
#include <Hash.h>

#include "config.h"

#define BUFFER_SIZE 80
#define BAUDRATE 115200  // don't go over 115200!
#define PROBEPIN1 D1
#define PROBEPIN2 D2
#define N 10 // Framing rate (points per frame)

ESP8266WiFiMulti WiFiMulti;
WebSocketsClient webSocket;

char inBuffer[BUFFER_SIZE];   // data from serial line
char JSONBuffer[800];         // data translated into JSON format

// This can loop forever if an endMarker is not found!
int fillBuffer() {
    int i = 0;
    char endMarker = 'Q';
    char rc=' ';

// Skip \n and eol 
    Serial.available(); Serial.read();
    Serial.available(); Serial.read();
// Start retrieving a single buffer (one line, with timestamp and six values)
    inBuffer[i]='\0';
// Read until end marker reached (presently Q character)
    while ( rc != endMarker ) {
// Read all available characters (but break if end marker is found
      while (Serial.available() > 0 ) {
          rc = Serial.read();
// If serial input exceeds buffer length skip all until next endMarker
// and return 0 
          if ( i >= BUFFER_SIZE ){
            while (Serial.read() != endMarker) {}
            return 0;
          }
// Place character in string if not end marker
          if (rc != endMarker) {
              inBuffer[i++] = rc;
          }
// Otherwise close the string and returns the buffer length
          else 
          {
            inBuffer[i] = '\0'; // terminate the string
            return i;
          }
      }
    }
}

boolean getChunk(char *buf, int a, int b) {
  for (int idx=a;idx<=b;idx++) { 
    *(buf++)=inBuffer[idx];
  }
  *buf='\0';
}

/*
 * The packet from Arduino is encoded in human readable ASCII
 * for debugging could be easely translated into binary.
 * The format is
 * 00000000001111111111222222222233333333
 * 01234567890123456789012345678901234567
 * ======================================
 * h   m  s  m   i   i   i   i   i   i
 * o   i  e  s   n   n   n   n   n   n   
 * u   n  c  e   1   2   3   4   5   6
 * r         c
 * ======================================
 * hhh:mm:ss.sss dddddddddddddddddddddddd
 * ======================================
 * It is translated into a JSON object containing a timestamp
 * encoded as a float with three decimal digits, and an array
 * of six floats in range [-1,1] with three decimal digits. The
 * The maximum value is of 1000 hours (minus one sec),
 * corresponding to more than 41 days.
 */
int formatJSON(int ptr) {
  char str_hour[4];
  char str_min[3];
  char str_sec[3];
  char str_msec[4];
  char str_data[6][5];
  unsigned long sec;
  getChunk(str_hour,0,2);
  getChunk(str_min,4,5);
  getChunk(str_sec,7,8);
  getChunk(str_msec,10,12);
  for (int i=0;i<6;i++){
    getChunk(str_data[i],14+(3*i),16+(3*i));
  }
  sec=60*(60*atoi(str_hour)+atoi(str_min))+atoi(str_sec);
  sprintf(JSONBuffer+ptr,"{\"t\":\"%lu.%s\",\"y\":[\"%.3f\",\"%.3f\",\"%.3f\",\"%.3f\",\"%.3f\",\"%.3f\"]},",
    sec,
    str_msec,
    (atoi(str_data[0])-500.0)/500.0,
    (atoi(str_data[1])-500.0)/500.0,
    (atoi(str_data[2])-500.0)/500.0,
    (atoi(str_data[3])-500.0)/500.0,
    (atoi(str_data[4])-500.0)/500.0,
    (atoi(str_data[5])-500.0)/500.0);
//  Serial.println(strlen(JSONBuffer));
  return strlen(JSONBuffer);
}

void webSocketEvent(WStype_t type, uint8_t * payload, size_t length) {
  const int packetLength=32;
  int count=0; // counter for led flash
  const int period=25; // flash period (n. of loops)
  int ptr=0;
  
	switch(type) {
		case WStype_DISCONNECTED:
			Serial.printf("Websocket disconnected!\n");
			break;
// The MCU remains forever in this "case": while (true) inside
		case WStype_CONNECTED:
			Serial.printf("Websocket connected on path %s\n", payload);
      while (true) {
        JSONBuffer[0]='['; // The frame is an array of values
        ptr=1;
        for (int n=0; n<N; n++) {
// If packet is not of expected length, discard and continue

        digitalWrite(PROBEPIN2,HIGH);
          if (fillBuffer() != packetLength) {
  //          Serial.print("skip:");
  //          Serial.println(strlen(inBuffer));
  //          Serial.print(" ");
  //          Serial.println(inBuffer);
            continue;
          }
        digitalWrite(PROBEPIN2,LOW);
  // Prepare the JSON to feed the server
          ptr=formatJSON(ptr);
  //        Serial.println(strlen(inBuffer));
  //        Serial.println(n);
  //        Serial.println(JSONBuffer);
        }
        JSONBuffer[ptr-1]=']'; // close the array
// send message to server
// Serial.println(JSONBuffer);
        digitalWrite(PROBEPIN1,HIGH);
        webSocket.sendTXT(JSONBuffer);
        digitalWrite(PROBEPIN1,LOW);
// Flash the led every period
        digitalWrite(LED_BUILTIN,HIGH);
        if (count++ == period) {
          count=0;
          digitalWrite(LED_BUILTIN,LOW);
        }
      }
  	  break;
		case WStype_TEXT:
			Serial.printf("[WSc] get text: %s\n", payload);
      delay(200);
			// send message to server
			webSocket.sendTXT("message here");
			break;
	}

}

void setup() {
// Initialize serial from Arduino
	Serial.begin(BAUDRATE);
	Serial.setDebugOutput(true);
	Serial.println();
  Serial.println();
  Serial.println();
	Serial.print("Booting");
	for(char t = 4; t > 0; t--) {
		Serial.print(".");
		Serial.flush();
		delay(1000);
	}
  Serial.println('\n');
  Serial.flush();
// Initialize LED (ON: waiting connection: FLASHING: sending to server)  
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(PROBEPIN1,OUTPUT);
  pinMode(PROBEPIN2,OUTPUT);
  digitalWrite(LED_BUILTIN,LOW); // Led connected to Vcc, pin sink mode
// Put here routine to get config values in AP mode
//  WiFi.mode(WIFI_AP);
// Join to local IP (tethering OK)
	WiFi.mode(WIFI_STA);
  WiFiMulti.addAP(ESSID,WIFI_PSK);
	while(WiFiMulti.run() != WL_CONNECTED) {
		delay(100);
	}
  Serial.print("Connecting to ");Serial.print(ekgServerIP);Serial.print(":");Serial.println(ekgServerPort);
// Connect to server websocket
  webSocket.begin(ekgServerIP, ekgServerPort, ekgServerPath);
// Associate event handler
	webSocket.onEvent(webSocketEvent);
// set reconnect timer
  webSocket.setReconnectInterval(1000);
}

void loop() {
	webSocket.loop();
}
