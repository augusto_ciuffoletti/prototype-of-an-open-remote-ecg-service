from flask import Flask, render_template
from flask_sockets import Sockets
import collections

app = Flask(__name__)
sockets = Sockets(app)

ws_list = {}

@sockets.route('/in/<userkey>')
def echo_socket(ws,userkey):
	print("Opening "+userkey+" input socket ")
	if userkey in ws_list:
		ws_list[userkey]['in']=ws
	else:
		ws_list[userkey]={'in': ws,'out': None}
	while not ws.closed:
		message = ws.receive()
#		print(message);
#		ws.send('ok') # send an ack
		if ws_list[userkey]['out'] is not None:
#			print("Receiver present!")
			while ws_list[userkey]['queue']:
				saved=ws_list[userkey]['queue'].popleft()
				ws_list[userkey]['out'].send(saved)
			ws_list[userkey]['out'].send(message)
		else:
#			print("skip")    # DEBUG ONLY
			if not ( 'queue' in ws_list ):
				ws_list[userkey]['queue']=collections.deque([])
			ws_list[userkey]['queue'].append(message)

@sockets.route('/out/<userkey>')
def test_socket(ws,userkey):
	print("Opening "+userkey+" output socket")
	if userkey in ws_list:
		ws_list[userkey]['out']=ws
		print("OUT open")
		## BRUTTISSIMO...
		while not ws.closed:
			message = ws.receive()
			print("preso")
		print("Websocket closed by peer")
		ws_list[userkey]['out']=None
	else:
		print ("Fail: user "+userkey+" not connected")
		ws.close()

@app.route('/')
def nokey():
	return "Please provide user key as URL path"

@app.route('/<userkey>')
def hello(userkey):
	return render_template("index.html",userkey=userkey)


if __name__ == "__main__":
	from gevent import pywsgi
	from geventwebsocket.handler import WebSocketHandler
	server = pywsgi.WSGIServer(('', 5000), app, handler_class=WebSocketHandler)
	server.serve_forever()
